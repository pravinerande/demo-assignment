app.controller('NavbarSearchCtrl', ['$scope', '$stateParams', '$filter', 'ResourceService', 'ModalService',
    function ($scope, $stateParams, $filter, ResourceService, ModalService) {
        $scope.quotes = [];
        var deepcopyQuotes = [];
        $scope.quoteImg = 'images/quote.jpg';
        $scope.progress = false;
        $scope.keyword = $stateParams.keyword;
        function getFilteredQuotes() {
            $scope.quotes = $filter('filter')(deepcopyQuotes, $stateParams.keyword);
            console.log($scope.quotes);
            $scope.progress = false;
        }
        function getQuotes() {
            $scope.progress = true;
            ResourceService.getQuotes().then(function (response) {
                deepcopyQuotes = response.data;
                getFilteredQuotes();
            }, function (error) {
                $scope.progress = false;
                alert("Error while fecthing quotes data" + error);
            });
        }
        getQuotes();
        $scope.showAuthorDetails = function (author) {
            ModalService.showModal(author);
        };
    }]);