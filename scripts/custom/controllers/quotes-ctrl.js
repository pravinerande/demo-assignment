app.controller('QuotesCtrl', ['$scope', 'ResourceService', function ($scope, ResourceService) {
        $scope.quotes = [];
        $scope.quoteImg = 'images/quote.jpg';
        $scope.progress = false;
        function getQuotes() {
            $scope.progress = true;
            ResourceService.getQuotes().then(function (response) {
                $scope.quotes = response.data;
                $scope.progress = false;
            }, function (error) {
                $scope.progress = false;
                alert("Error while fecthing quotes data" + error);
            });
        }
        getQuotes();
    }]);