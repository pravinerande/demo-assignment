app.controller('AuthorsCtrl', ['$scope', '$filter', 'ResourceService', 'ModalService',
    function ($scope, $filter, ResourceService, ModalService) {
        $scope.quotes = [];
        $scope.silderQuotes = [];
        $scope.lettersArray = [];
        var deepCopyQuotes = [];
        $scope.quoteImg = 'images/quote.jpg';
        $scope.slideInterval = 10000;
        $scope.progress = false;
        function genCharArray(charA, charZ) {
            for (var i = charA.charCodeAt(0); i <= charZ.charCodeAt(0); i++) {
                $scope.lettersArray.push(String.fromCharCode(i));
            }
        }
        genCharArray('A', 'Z');
        function getQuotes() {
            $scope.progress = true;
            ResourceService.getQuotes().then(function (response) {
                $scope.quotes = response.data;
                deepCopyQuotes = angular.copy($scope.quotes);
                $scope.silderQuotes = deepCopyQuotes;
                $scope.progress = false;
            }, function (error) {
                $scope.progress = false;
                alert("Error while fecthing quotes data" + error);
            });
        }
        $scope.linkAuthor = function (letter) {
            if (letter)
                $scope.quotes = $filter('filterLetter')(deepCopyQuotes, letter);
            else
                $scope.quotes = deepCopyQuotes;
            console.log($scope.quotes);
        };
        $scope.showAuthorDetails = function (author) {
            ModalService.showModal(author);
        };
        getQuotes();
    }]);
