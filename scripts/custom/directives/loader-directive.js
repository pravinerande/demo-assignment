app.directive('loading', function () {
    return {
        restrict: 'E',
        replace: true,
        template: '<div class="loading text-center"><img class="loader-img" src="images/loader.gif"/></div>',
        link: function (scope, element, attr) {
            scope.$watch('progress', function (val) {
                if (val)
                    element.removeClass('ng-hide');
                else
                    element.addClass('ng-hide');
            });
        }
    };
});