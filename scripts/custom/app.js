'use strict';

var dependency = [
    'ui.router',
    'ui.bootstrap'
];
var app = angular.module('demoApp', dependency);

app.config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("/quotes");
    $stateProvider
            .state('quotes', {
                url: '/quotes',
                templateUrl: 'views/quotes.html',
                controller: 'QuotesCtrl'
            })
            .state('authors', {
                url: '/authors',
                templateUrl: 'views/authors.html',
                controller: 'AuthorsCtrl'
            })
            .state('searchResult', {
                url: '/search/result/:keyword',
                templateUrl: 'views/search-result.html',
                controller: 'NavbarSearchCtrl'
            });
});
app.run(['$rootScope', function ($rootScope) {
        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
            console.log("State transfer to=>" + toState);
            console.log("State transfer from=>" + fromState);
        });
    }]);
