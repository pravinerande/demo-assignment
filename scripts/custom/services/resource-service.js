app.service('ResourceService', ['$http', function ($http) {
        this.getQuotes = function () {
            return $http.get('scripts/custom/json/quotes.json');
        };
    }]);
app.service('ModalService', ['$modal', function ($modal) {
        this.showModal = function (data) {
            var modalInstance = $modal.open({
                templateUrl: 'views/author-details.html',
                backdrop: 'static',
                controller: ['$scope', '$modalInstance', 'author', function ($scope, $modalInstance, author) {
                        $scope.author = author;
                        $scope.close = function () {
                            $modalInstance.dismiss('close');
                        };
                    }],
                resolve: {
                    author: function () {
                        return  data;
                    }
                }
            });
            modalInstance.result.then(function () {
                //To execution when modal return something
            }, function () {
                console.log('Modal dismissed at: ' + new Date());
            });
        };
    }]);